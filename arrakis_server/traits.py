# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

from collections.abc import Iterable
from typing import Protocol, runtime_checkable

from pyarrow import flight
from typing_extensions import TypeGuard


@runtime_checkable
class ServerBackend(Protocol):
    def do_stream(
        self, *, channels: Iterable[str], start: int, end: int
    ) -> flight.FlightDataStream: ...

    def do_describe(self, *, channels: Iterable[str]) -> flight.FlightDataStream: ...

    def do_find(
        self, *, pattern: str, data_type: str, min_rate: int, max_rate: int
    ) -> flight.FlightDataStream: ...

    def do_count(
        self, *, pattern: str, data_type: str, min_rate: int, max_rate: int
    ) -> flight.FlightDataStream: ...

    def finalize(self) -> None: ...


@runtime_checkable
class PublishServerBackend(ServerBackend, Protocol):
    def do_publish(self, *, producer_id: str) -> flight.FlightDataStream: ...

    def do_partition(self, reader, writer, *producer_id: str) -> None: ...


def can_publish(
    backend: ServerBackend | PublishServerBackend,
) -> TypeGuard[PublishServerBackend]:
    """Determine if a server backend supports publish-like functionality."""
    return isinstance(backend, PublishServerBackend)
