import argparse
import logging
import pathlib
import sys

from . import DEFAULT_LOCATION, __version__
from .backends import BackendType
from .server import FlightServer

logger = logging.getLogger("arrakis")


def get_log_level(args):
    """Determine the log level from logging options."""
    if args.quiet:
        return logging.WARNING
    elif args.verbose:
        return logging.DEBUG
    else:
        return logging.INFO


def format_args(args):
    """Format CLI arguments to feed into Flight server."""
    kwargs = vars(args)
    kwargs.pop("verbose")
    kwargs.pop("quiet")
    return kwargs


def main():
    parser = argparse.ArgumentParser(prog="arrakis-server")
    parser.add_argument(
        "--version",
        action="version",
        version=__version__,
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-q",
        "--quiet",
        action="store_true",
        help="If set, only display warnings and errors.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="If set, display additional logging messages.",
    )
    parser.add_argument(
        "-u",
        "--url",
        default=DEFAULT_LOCATION,
        help=f"Serve requests at this URL. Defaults to {DEFAULT_LOCATION}",
    )
    parser.add_argument(
        "-b",
        "--backend",
        type=str.upper,
        choices=BackendType.__members__,
        default=BackendType.MOCK.name,
        help=f"The data backend to use. Default: {BackendType.MOCK.name}",
    )
    parser.add_argument(
        "--backend-server-url",
        help=(
            "URL pointing to a running backend server. "
            "Required if using CLICKHOUSE, HYBRID, or NDS2 backend."
        ),
    )
    parser.add_argument(
        "--mock-channel-file",
        action="append",
        type=pathlib.Path,
        help=(
            "Channel definition file for MOCK backend. May be specified multiple times."
        ),
    )
    args = parser.parse_args()

    # validate arguments
    match BackendType[args.backend.upper()]:
        case BackendType.CLICKHOUSE | BackendType.HYBRID | BackendType.NDS2:
            if not args.backend_server_url:
                parser.error(
                    "CLICKHOUSE, HYBRID, and NDS2 backends "
                    "require --backend-server-url."
                )

    # set up logger
    logger = logging.getLogger("arrakis")
    log_level = get_log_level(args)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(log_level)
    formatter = logging.Formatter("%(asctime)s | arrakis : %(levelname)s : %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # serve requests
    server = FlightServer(**format_args(args))
    try:
        server.serve()
    finally:
        server.finalize()


if __name__ == "__main__":
    main()
