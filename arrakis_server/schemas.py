# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

"""Arrow Flight schema definitions."""

from collections.abc import Iterable

import numpy
import pyarrow


def stream(channels: Iterable[str], dtypes: Iterable[str]) -> pyarrow.Schema:
    """Create an Arrow Flight schema for `stream`.

    Parameters
    ----------
    channels : Iterable[str]
        The list of channels for the fetch request.
    dtypes : Iterable[str]
        The list of data types, one per channel.

    Returns
    -------
    pyarrow.Schema
        The stream schema.

    """
    columns = [pyarrow.field("time", pyarrow.int64(), nullable=False)]
    pyarrow_dtypes = [pyarrow.from_numpy_dtype(numpy.dtype(dtype)) for dtype in dtypes]
    for channel, dtype in zip(channels, pyarrow_dtypes):
        columns.append(pyarrow.field(channel, pyarrow.list_(dtype), nullable=False))
    return pyarrow.schema(columns)


def describe() -> pyarrow.Schema:
    """Create an Arrow Flight schema for `describe`.

    Returns
    -------
    pyarrow.Schema
        The describe schema.

    """
    return find()


def find() -> pyarrow.Schema:
    """Create an Arrow Flight schema for `find`.

    Returns
    -------
    pyarrow.Schema
        The find schema.

    """
    return pyarrow.schema(
        [
            pyarrow.field("channel", pyarrow.string()),
            pyarrow.field("data_type", pyarrow.string()),
            pyarrow.field("sample_rate", pyarrow.int32()),
            pyarrow.field("partition_id", pyarrow.string()),
        ]
    )


def count() -> pyarrow.Schema:
    """Create an Arrow Flight schema for `count`.

    Returns
    -------
    pyarrow.Schema
        The count schema.

    """
    return pyarrow.schema([pyarrow.field("count", pyarrow.int64())])


def partition() -> pyarrow.Schema:
    """Create an Arrow Flight schema for `partition`.

    Returns
    -------
    pyarrow.Schema
        The partition schema.

    """
    return pyarrow.schema(
        [
            pyarrow.field("channel", pyarrow.string(), nullable=False),
            pyarrow.field("data_type", pyarrow.string(), nullable=False),
            pyarrow.field("sample_rate", pyarrow.int32(), nullable=False),
            pyarrow.field("partition_id", pyarrow.string()),
        ]
    )


def publish() -> pyarrow.Schema:
    """Create an Arrow Flight schema for `publish`.

    Returns
    -------
    pyarrow.Schema
        The publish schema.

    """
    dtype = pyarrow.map_(pyarrow.string(), pyarrow.string())
    return pyarrow.schema([pyarrow.field("properties", dtype, nullable=False)])
