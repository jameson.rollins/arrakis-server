# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

import importlib.resources
import logging
import re
import time
from collections.abc import Callable, Iterable, Iterator
from pathlib import Path
from typing import TypeAlias

import gpstime
import numpy
import pyarrow
import toml
from arrakis import Channel, Time
from pyarrow import flight

from ... import schemas
from .. import utils
from . import channels as channel_lists

logger = logging.getLogger("arrakis")


ArrayTransform: TypeAlias = Callable[[numpy.ndarray], numpy.ndarray]


def _func_random_normal(t):
    return numpy.random.normal(size=len(t))


def load_channels(*paths: Path) -> tuple[dict[str, Channel], dict[str, ArrayTransform]]:
    """load channel description TOML files

    Returns a dictionary of channel: channel_obj.

    Channels should be defined in tables, with the channel name in the
    header.  The table should include:

      `rate`  in samples per second
      `dtype` as a python dtype
      `func`  an optional function to generate the data, will be given
              the block time array as it's single argument
              (numpy.random.uniform used by default)

    example:

    ["MY:CHANNEL-NAME"]
    rate = 16384
    dtype = "float32"
    fun = "numpy.cos"

    """
    channel_map = {}
    channel_func_map = {}
    for path in paths:
        logger.info("loading channel description file: %s", path)
        channel_list = toml.load(path)
        for channel_name, meta in channel_list.items():
            logger.debug("  %s", channel_name)
            channel = Channel.from_name(
                channel_name,
                sample_rate=meta["rate"],
                data_type=numpy.dtype(meta["dtype"]),
            )
            if "func" in meta:
                func = eval(meta["func"])
            else:
                func = _func_random_normal
            channel_map[channel_name] = channel
            channel_func_map[channel_name] = func
    return channel_map, channel_func_map


class MockBackend:
    """Mock server backend that generates synthetic timeseries data."""

    def __init__(self, channel_files: list[Path] | None = None):
        """Initialize mock server with list of channel definition files."""
        if not channel_files:
            channel_files = [
                Path(
                    str(importlib.resources.path(channel_lists, f"{ifo}_channels.toml"))
                )
                for ifo in ("H1", "L1")
            ]
        self._channel_map, self._channel_func_map = load_channels(*channel_files)

    def do_describe(self, *, channels: Iterable[str]) -> flight.FlightDataStream:
        """Retrieve metadata for the 'find' route."""
        metadata = [self._channel_map[channel] for channel in channels]
        return utils.create_metadata_stream(metadata)

    def do_find(self, **kwargs) -> flight.FlightDataStream:
        """Retrieve metadata for the 'find' route."""
        channels = self._match_channels(**kwargs)
        return utils.create_metadata_stream(channels)

    def do_count(self, **kwargs) -> flight.FlightDataStream:
        """Retrieve metadata for the 'count' route."""
        count = len(self._match_channels(**kwargs))
        schema = schemas.count()
        batch = pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array(
                    [count],
                    type=schema.field("count").type,
                ),
            ],
            schema=schema,
        )
        return flight.RecordBatchStream(
            pyarrow.RecordBatchReader.from_batches(schema, [batch])
        )

    def do_stream(
        self, *, channels: Iterable[str], start: int, end: int
    ) -> flight.FlightDataStream:
        """Retrieve timeseries data for the 'fetch' route."""
        # Generate data from requested channels
        dtypes = [
            utils.channel_to_dtype_name(self._channel_map[channel])
            for channel in channels
        ]
        schema = schemas.stream(channels, dtypes)

        is_live = not start and not end
        if is_live:
            return flight.GeneratorStream(
                schema, self._generate_live_series(schema, channels)
            )
        else:
            return flight.GeneratorStream(
                schema, self._generate_series(schema, channels, start, end)
            )

    def finalize(self) -> None:
        pass

    def _match_channels(
        self, *, pattern: str, data_type: str, min_rate: int, max_rate: int
    ) -> list[Channel]:
        if min_rate is None:
            min_rate = 0
        if max_rate is None:
            max_rate = 65536

        expr = re.compile(pattern)
        channels = []
        for chan in self._channel_map.values():
            if expr.match(str(chan)):
                rate = chan.sample_rate
                assert rate is not None
                if not (rate >= min_rate and rate <= max_rate):
                    continue
                if data_type and numpy.dtype(data_type) != chan.data_type:
                    continue
                channels.append(chan)

        return channels

    def _generate_block(
        self, schema: pyarrow.Schema, channels: Iterable[str], timestamp: int
    ) -> pyarrow.RecordBatch:
        channel_data = []
        for channel in channels:
            rate = self._channel_map[channel].sample_rate
            assert rate is not None
            size = rate // 16
            dtype = self._channel_map[channel].data_type
            time_array = timestamp + numpy.arange(size) / rate
            func = self._channel_func_map[channel]
            data = numpy.array(
                func(time_array),
                dtype=dtype,
            )
            channel_data.append(
                pyarrow.array(
                    [data],
                    type=schema.field(channel).type,
                )
            )

        return pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array(
                    [timestamp],
                    type=schema.field("time").type,
                ),
                *channel_data,
            ],
            schema=schema,
        )

    def _generate_series(
        self, schema: pyarrow.Schema, channels: Iterable[str], start: int, end: int
    ) -> Iterator[pyarrow.RecordBatch]:
        dt = int((1 / 16) * Time.SECONDS)
        current = int(start * 1e9)

        # generate data
        while current < int(end * 1e9):
            yield self._generate_block(schema, channels, current)
            current += dt

    def _generate_live_series(
        self, schema: pyarrow.Schema, channels: Iterable[str]
    ) -> Iterator[pyarrow.RecordBatch]:
        dt = int((1 / 16) * Time.SECONDS)
        current = (int(gpstime.gpsnow() * Time.SECONDS) // dt) * dt

        # generate live data continuously
        while True:
            yield self._generate_block(schema, channels, current)
            current += dt

            # sleep for up to dt to simulate live stream
            time.sleep(
                max((current - int(gpstime.gpsnow() * Time.SECONDS)) / Time.SECONDS, 0)
            )
