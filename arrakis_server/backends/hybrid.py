# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

import heapq
import re
import sys
import threading
from collections import defaultdict
from collections.abc import Generator, Iterable, Iterator
from datetime import timedelta
from pathlib import Path
from typing import Any

import appdirs
import gpstime
import numpy
import pyarrow
import toml
from arrakis import Channel, SeriesBlock, Time
from confluent_kafka import Consumer, TopicPartition
from pyarrow import flight

from .. import schemas
from ..partition import partition_channels
from . import utils

DEFAULT_TIMEOUT = timedelta(seconds=1)


class HybridBackend:
    """Server backend serving timeseries data from Kafka."""

    def __init__(self, server_url: str | None):
        assert isinstance(server_url, str)
        self._server_url = server_url
        self._metadata = self._load_metadata()
        self._channels = {channel for channel in self._metadata.values()}
        self._lock = threading.Lock()

    def do_describe(self, *, channels: Iterable[str]) -> flight.FlightDataStream:
        """Retrieve metadata for the 'find' route."""
        metadata = [self._metadata[channel] for channel in channels]
        return utils.create_metadata_stream(metadata)

    def do_find(self, **kwargs) -> flight.FlightDataStream:
        """Retrieve metadata for the 'find' route."""
        channels = self._match_channels(**kwargs)
        return utils.create_metadata_stream(channels)

    def do_publish(self, *, producer_id: str) -> flight.FlightDataStream:
        """Retrieve connection info for the 'publish' route."""
        # FIXME: incorporate the producer ID with auth verification
        schema = schemas.publish()
        info = {"bootstrap.servers": self._server_url}
        batch = pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array(
                    [info],
                    type=schema.field("properties").type,
                ),
            ],
            schema=schema,
        )
        return flight.RecordBatchStream(
            pyarrow.RecordBatchReader.from_batches(schema, [batch])
        )

    def do_partition(self, reader, writer, *, producer_id: str) -> None:
        """Retrieve metadata for the 'partition' route."""
        with self._lock:
            schema = schemas.partition()

            # read metadata from client
            channels = []
            for batch in read_all_chunks(reader):
                for meta in batch.to_pylist():
                    data_type = numpy.dtype(meta["data_type"])
                    channel = Channel.from_name(
                        meta["channel"],
                        sample_rate=meta["sample_rate"],
                        data_type=data_type,
                    )
                    channels.append(channel)

            # assign any new partitions
            changed = set(channels) - self._channels
            if changed:
                self._metadata = partition_channels(
                    list(changed),
                    max_channels=100,
                    producer_id=producer_id,
                    partitions=self._metadata,
                )
                channels = [channel for channel in self._metadata.values()]
                self._channels = set(channels)

            # prepare the batch with mappings
            batch = utils.create_metadata_batch(channels)

        # send partitions back to the client
        writer.begin(schema)
        writer.write_batch(batch)
        writer.close()

    def do_count(self, **kwargs) -> flight.FlightDataStream:
        """Retrieve metadata for the 'count' route."""
        count = len(self._match_channels(**kwargs))
        schema = schemas.count()
        batch = pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array(
                    [count],
                    type=schema.field("count").type,
                ),
            ],
            schema=schema,
        )
        return flight.RecordBatchStream(
            pyarrow.RecordBatchReader.from_batches(schema, [batch])
        )

    def do_stream(
        self, *, channels: Iterable[str], start: int, end: int
    ) -> flight.FlightDataStream:
        """Retrieve timeseries data for the 'stream' route."""
        # Query for channel metadata
        metadata = [self._metadata[channel] for channel in channels]
        dtypes = [utils.channel_to_dtype_name(channel) for channel in metadata]
        schema = schemas.stream(channels, dtypes)

        is_live = not start and not end
        if is_live:
            return flight.GeneratorStream(schema, self._generate_live_series(metadata))
        else:
            raise NotImplementedError

    def finalize(self) -> None:
        cache_dir = Path(appdirs.user_cache_dir("arrakis", "server"))
        partition_file = cache_dir / "partitions.toml"
        partitions = {}
        for channel_name, meta in self._metadata.items():
            partitions[channel_name] = {
                "rate": meta.sample_rate,
                "dtype": numpy.dtype(meta.data_type).name,
                "partition": meta.partition_id,
            }
        with partition_file.open("w") as f:
            toml.dump(partitions, f)

    def _load_metadata(self) -> dict[str, Channel]:
        metadata = {}
        cache_dir = Path(appdirs.user_cache_dir("arrakis", "server"))
        cache_dir.mkdir(exist_ok=True)
        partition_file = cache_dir / "partitions.toml"
        if partition_file.exists():
            with partition_file.open("r") as f:
                partitions = toml.load(f)
                for channel_name, meta in partitions.items():
                    data_type = numpy.dtype(meta["dtype"])
                    channel = Channel.from_name(
                        channel_name,
                        data_type=data_type,
                        sample_rate=meta["rate"],
                        partition_id=meta["partition"],
                    )
                    metadata[channel_name] = channel
        return metadata

    def _match_channels(
        self, *, pattern: str, data_type: str, min_rate: int, max_rate: int
    ) -> list[Channel]:
        if min_rate is None:
            min_rate = 0
        if max_rate is None:
            max_rate = 65536

        expr = re.compile(pattern)
        channels = []
        for channel in self._metadata.values():
            if expr.match(str(channel)):
                rate = channel.sample_rate
                assert rate is not None
                if not (rate >= min_rate and rate <= max_rate):
                    continue
                if data_type and numpy.dtype(data_type) != channel.data_type:
                    continue
                channels.append(channel)

        return channels

    def _generate_live_series(
        self, channels: Iterable[Channel]
    ) -> Iterator[pyarrow.RecordBatch]:
        dt = int((1 / 16) * Time.SECONDS)
        start = (int(gpstime.gpsnow() * Time.SECONDS) // dt) * dt

        # generate live data continuously
        partitions = {
            name: channel.partition_id
            for name, channel in self._metadata.items()
            if channel.partition_id
        }
        with Connection(self._server_url, partitions, channels) as conn:
            for buf in conn.read(start=start):
                yield buf


class Muxer:
    """Track and multiplex buffers from multiple channels."""

    def __init__(
        self,
        channels: Iterable,
        partition_map: dict[str, list],
        start: int | None = None,
        max_latency: timedelta = DEFAULT_TIMEOUT,
    ):
        """Create a muxer to track and multiplex buffers.

        Parameters
        ----------
        channels: iterable
            The channels requested.
        start : numeric, optional
            The GPS time to start muxing buffers for.
            If not set, accept buffers from any time.
        max_latency : timedelta, optional
            The maximum latency to wait for messages from missing channels
            before multiplexing. Default is 1 second.

        """
        self._max_latency = max_latency
        self._partition_map = partition_map
        self._buffers: dict[int, dict[str, Any]] = defaultdict(dict)
        self._times: list[int] = []
        self._start = start
        self._last_time = start

        # track when processing started to handle lookback properly
        self._processing_start_time = int(gpstime.gpsnow() * Time.SECONDS)

        # generate channels and keys needed to track
        self._channels = [str(ch) for ch in channels]
        self._partition_map = partition_map
        self._keys = {partition for partition in partition_map.keys()}

    def push(self, key: str, buf):
        """Push a buffer into the muxer.

        Parameters
        ----------
        key: str
            The key associated with this buffer.
        buf : dict
            The buffer to add to the muxer.

        """
        time = buf.column("time").to_numpy()[0]

        if not self._last_time:
            self._last_time = time

        # skip over buffers that have already been pulled
        if time < self._last_time:
            return

        # add buffer
        if time in self._buffers:
            if key not in self._buffers[time]:
                self._buffers[time][key] = buf
        else:
            heapq.heappush(self._times, time)
            self._buffers[time] = {key: buf}

    def pull(self) -> Iterator[SeriesBlock]:
        """Pull buffers from the muxer.

        Yields
        ------
        buffers : dict of Buffers, keyed by str
            buffers, keyed by channel name

        """
        if not self._times:
            return

        # yield buffers as long as one of two conditions are met:
        # (1) oldest buffer is stale (older than latency cutoff)
        # (2) oldest buffer is full (all channels requested)
        time = self._times[0]
        while self._is_buffer_full(time) or self._is_buffer_stale(time):
            yield self._format_buffer(self._buffers.pop(time))
            self._last_time = heapq.heappop(self._times)
            if not self._times:
                break
            time = self._times[0]

    def _is_buffer_stale(self, time):
        """Check if a buffer time is older than the latency cutoff."""
        time_now = gpstime.gpsnow()
        dt_lookback = max(self._processing_start_time - time, 0) / float(Time.SECONDS)
        dt_timeout = self._max_latency.total_seconds()
        oldest_time_allowed = time_now - dt_lookback - dt_timeout
        return time <= int(oldest_time_allowed * Time.SECONDS)

    def _is_buffer_full(self, time):
        """Check if a buffer time has all channels requested."""
        return len(self._buffers[time]) == len(self._keys)

    def _format_buffer(self, batches: dict):
        """Format batches into a single batch"""
        time = 0
        fields = {}
        arrays = {}
        for batch in batches.values():
            time = batch.column("time").to_numpy()[0]

            # extract data/fields from batch
            batch_fields: list[pyarrow.field] = [field for field in batch.schema][1:]
            for field in batch_fields:
                channel = field.name
                fields[channel] = field
                arrays[channel] = batch.column(channel)

        # realign batch for channel order requested
        schema = pyarrow.schema([fields[channel] for channel in self._channels])
        return pyarrow.RecordBatch.from_arrays(
            [
                pyarrow.array([time], type=schema.field("time").type),
                *[arrays[channel] for channel in self._channels],
            ],
            schema=schema,
        )


class Connection:
    """A connection object to read data from Kafka."""

    def __init__(self, server: str, partitions: dict[str, str], channels: Iterable):
        """Create a Kafka connection.

        Parameters
        ----------
        server : str
            The Kafka broker to connect to.
        channels : iterable
            The channels requested.

        """
        self._channels = list(channels)
        self._partitions = partitions

        # group channels by partitions
        self._partition_map = defaultdict(list)
        for channel in channels:
            if channel in partitions:
                partition = partitions[channel]
                self._partition_map[partition].append(channel)

        # create Kafka consumer
        consumer_settings = {
            "bootstrap.servers": server,
            "group.id": "some.groupid",
            "message.max.bytes": 10_000_000,  # 10 MB
            "enable.auto.commit": False,
            "auto.offset.reset": "LATEST",
        }
        self._consumer = Consumer(consumer_settings)
        self._topics = [
            f"arrakis-{partition}" for partition in self._partition_map.keys()
        ]

    def read(
        self,
        start: int | None = None,
        max_latency: timedelta = DEFAULT_TIMEOUT,
        poll_timeout: timedelta = DEFAULT_TIMEOUT,
    ) -> Generator[pyarrow.RecordBatch, None, None]:
        """Read buffers from Kafka. Requires an open connection.

        Parameters
        ----------
        start : numeric, optional
            The GPS time to start receiving buffers for.
            Defaults to 'now'.
        max_latency : timedelta, optional
            The maximum latency to wait for messages.
            Default is 1 second.
        poll_timeout : timedelta, optional
            The maximum time to wait for a single message from Kafka.
            Default is 1 second.

        Yields
        ------
        buffers : dict of Buffers, keyed by str
            buffers, keyed by channel name

        """
        # if start time is specified, adjust the consumer's
        # offset to point at the data requested
        if start:
            # convert to UNIX time in ms
            offset_time = int(gpstime.gps2unix(start) * 1000)
            # get offsets corresponding to times
            partitions = [
                TopicPartition(topic, partition=0, offset=offset_time)
                for topic in self._topics
            ]
            partitions = self._consumer.offsets_for_times(partitions)
            # reassign topic partitions to consumer
            self._consumer.unsubscribe()
            self._consumer.assign(partitions)
        else:
            offset_time = 0

        # set up muxer to multiplex buffers
        self._muxer = Muxer(
            self._channels, self._partition_map, start=start, max_latency=max_latency
        )

        # consume buffers from Kafka
        try:
            while True:
                msg = self._consumer.poll(timeout=poll_timeout.total_seconds())
                if msg and not msg.error():
                    # deserialize message then add to muxer
                    # and pull time-ordered buffers from it
                    partition = msg.topic().split("-")[1]
                    print(partition, file=sys.stderr)
                    with pyarrow.ipc.open_stream(msg.value()) as reader:
                        for batch in read_all_batches(reader):
                            # downselect channels
                            batch = batch.select(
                                ["time", *self._partition_map[partition]]
                            )
                            self._muxer.push(partition, batch)
                            yield from self._muxer.pull()
        except Exception as e:
            print(e, file=sys.stderr)

    def __iter__(self) -> Generator[pyarrow.RecordBatch, None, None]:
        """Read buffers from Kafka. Requires an open connection.

        Calls read() with default parameters.

        """
        yield from self.read()

    def open(self):
        """Open a connection to Kafka, subscribing to all required topics."""
        self._consumer.subscribe(self._topics)

    def close(self):
        """Closes a connection to Kafka, unsubscribing from all topics."""
        self._consumer.unassign()
        self._consumer.unsubscribe()
        self._consumer.close()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, *args):
        self.close()


def read_all_chunks(
    reader: pyarrow.flight.MetadataRecordBatchReader,
) -> Iterator[pyarrow.RecordBatch]:
    while True:
        try:
            batch, _ = reader.read_chunk()
            yield batch
        except StopIteration:
            return


def read_all_batches(
    reader: pyarrow.ipc.RecordBatchStreamReader,
) -> Iterator[pyarrow.RecordBatch]:
    while True:
        try:
            yield reader.read_next_batch()
        except StopIteration:
            return
