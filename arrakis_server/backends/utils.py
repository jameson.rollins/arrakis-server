# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE


import pyarrow
from arrakis import Channel
from pyarrow import flight

from .. import schemas


def create_metadata_batch(channels: list[Channel]) -> pyarrow.RecordBatch:
    """Create a record batch from channel metadata."""
    schema = schemas.find()
    metadata = [
        (
            channel.name,
            channel_to_dtype_name(channel),
            channel.sample_rate,
            channel.partition_id,
        )
        for channel in channels
    ]
    if metadata:
        names, dtypes, rates, partitions = map(list, zip(*metadata))
    else:
        names, dtypes, rates, partitions = [], [], [], []
    return pyarrow.RecordBatch.from_arrays(
        [
            pyarrow.array(names, type=schema.field("channel").type),
            pyarrow.array(dtypes, type=schema.field("data_type").type),
            pyarrow.array(rates, type=schema.field("sample_rate").type),
            pyarrow.array(partitions, type=schema.field("partition_id").type),
        ],
        schema=schema,
    )


def create_metadata_stream(channels: list[Channel]) -> flight.RecordBatchStream:
    """Create a record batch stream from channel metadata."""
    return record_batch_to_stream(create_metadata_batch(channels))


def record_batch_to_stream(batch: pyarrow.RecordBatch) -> flight.RecordBatchStream:
    """Convert a record batch into a record batch stream."""
    return flight.RecordBatchStream(
        pyarrow.RecordBatchReader.from_batches(batch.schema, [batch])
    )


def channel_to_dtype_name(channel: Channel) -> str:
    """Given a channel, return the data type's name."""
    assert channel.data_type is not None
    return channel.data_type.name
