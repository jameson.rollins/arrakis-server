# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

from collections.abc import Iterable, Iterator

import nds2
import numpy
import pyarrow
from arrakis import Channel, Time
from pyarrow import flight

from .. import schemas
from . import utils

_NDS2_DATA_TYPE: dict[int, numpy.dtype] = {
    1: numpy.dtype(numpy.int16),
    2: numpy.dtype(numpy.int32),
    4: numpy.dtype(numpy.int64),
    8: numpy.dtype(numpy.float32),
    16: numpy.dtype(numpy.float64),
    64: numpy.dtype(numpy.uint32),
}


class NDS2Backend:
    """Server backend serving timeseries data from NDS2."""

    def __init__(self, server_url):
        self._server_url = server_url
        if ":" in server_url:
            host, port = server_url.split(":", 1)
            self._params = nds2.parameters(host, int(port))
        else:
            self._params = nds2.parameters(server_url)

    def do_find(
        self, *, pattern: str, data_type: str, min_rate: int, max_rate: int
    ) -> flight.FlightDataStream:
        """Retrieve metadata for the 'find' route."""
        nds2_channels = nds2.find_channels(
            pattern,
            nds2.channel.CHANNEL_TYPE_RAW | nds2.channel.CHANNEL_TYPE_ONLINE,
            min_sample_rate=min_rate,
            max_sample_rate=max_rate,
            params=self._params,
        )
        channels = [
            Channel.from_name(
                channel.name,
                data_type=_NDS2_DATA_TYPE[channel.data_type],
                sample_rate=int(channel.sample_rate),
            )
            for channel in nds2_channels
        ]
        return utils.create_metadata_stream(channels)

    def do_count(
        self, *, pattern: str, data_type: str, min_rate: int, max_rate: int
    ) -> flight.FlightDataStream:
        """Retrieve metadata for the 'count' route."""
        schema = schemas.count()
        conn = None
        try:
            conn = nds2.connection(self._params)
            count = conn.count_channels(
                pattern,
                nds2.channel.CHANNEL_TYPE_RAW | nds2.channel.CHANNEL_TYPE_ONLINE,
                nds2.channel.DEFAULT_DATA_MASK,
                min_rate,
                max_rate,
            )
            batch = pyarrow.RecordBatch.from_arrays(
                [
                    pyarrow.array(
                        [count],
                        type=schema.field("count").type,
                    ),
                ],
                schema=schema,
            )
            return flight.RecordBatchStream(
                pyarrow.RecordBatchReader.from_batches(schema, [batch])
            )
        finally:
            if conn is not None:
                conn.close()

    def do_describe(self, *, channels: Iterable[str]) -> flight.FlightDataStream:
        """Retrieve metadata for the 'describe' route."""
        return utils.create_metadata_stream(self._query_metadata(channels))

    def do_stream(
        self, *, channels: Iterable[str], start: int, end: int
    ) -> flight.FlightDataStream:
        """Retrieve timeseries data for the 'fetch' route."""
        # Query for channel metadata
        metadata = self._query_metadata(channels)
        dtypes = [utils.channel_to_dtype_name(channel) for channel in metadata]
        schema = schemas.stream(channels, dtypes)

        # stream data
        is_live = not start and not end
        if is_live:
            stream = nds2.iterate(
                channels, stride=nds2.connection.FAST_STRIDE, params=self._params
            )
        else:
            stream = nds2.iterate(
                channels,
                start,
                end,
                stride=nds2.connection.FAST_STRIDE,
                params=self._params,
            )
        return flight.GeneratorStream(schema, self._process_stream(stream, schema))

    def finalize(self) -> None:
        pass

    def _process_stream(self, stream, schema) -> Iterator[pyarrow.RecordBatch]:
        for buffers in stream:
            channel_data = []
            timestamp = 0
            for buf in buffers:
                timestamp = buf.gps_seconds * Time.SECONDS + buf.gps_nanoseconds
                channel_data.append(
                    pyarrow.array([buf.data], type=schema.field(buf.channel.name).type)
                )

            yield pyarrow.RecordBatch.from_arrays(
                [
                    pyarrow.array(
                        [timestamp],
                        type=schema.field("time").type,
                    ),
                    *channel_data,
                ],
                schema=schema,
            )

    def _query_metadata(self, channels: Iterable[str]) -> list[Channel]:
        buffers = next(nds2.iterate(channels, 0, 1, params=self._params))
        return [
            Channel.from_name(
                buf.channel.name,
                data_type=_NDS2_DATA_TYPE[buf.channel.data_type],
                sample_rate=buf.channel.sample_rate,
            )
            for buf in buffers
        ]

    @staticmethod
    def _extract_channel_metadata(
        batches: Iterable[pyarrow.RecordBatch],
    ) -> list[Channel]:
        channels = []
        for batch in batches:
            for _, entry in batch.to_pandas().iterrows():
                channel = Channel.from_name(
                    entry["channel"].decode("utf-8"),
                    data_type=numpy.dtype(entry["data_type"].decode("utf-8")),
                    sample_rate=entry["sample_rate"],
                )
                channels.append(channel)
        return channels
