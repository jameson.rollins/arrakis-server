# Copyright (c) 2022, California Institute of Technology and contributors
#
# You should have received a copy of the licensing terms for this
# software included in the file "LICENSE" located in the top-level
# directory of this package. If you did not, you can view a copy at
# https://git.ligo.org/ngdd/arrakis-server/-/raw/main/LICENSE

import logging
from collections.abc import Iterable, Iterator
from pathlib import Path

from arrakis.flight import RequestType, create_command, parse_command
from pyarrow import flight

from . import DEFAULT_LOCATION, schemas, traits
from .backends import BackendType

logger = logging.getLogger("arrakis")


class FlightServer(flight.FlightServerBase):
    """Arrow Flight server implementation to server timeseries.

    Parameters
    ----------
    url : str, optional
        The URL to connect to.
        If set, protocol must be set to grpc://.

    """

    def __init__(
        self,
        url: str = DEFAULT_LOCATION,
        *,
        backend: str = BackendType.CLICKHOUSE.name,
        backend_server_url: str | None = None,
        mock_channel_file: list[Path] | None = None,
        **kwargs,
    ):
        super().__init__(url, **kwargs)
        self._location = url
        self._backend: traits.ServerBackend | traits.PublishServerBackend
        logger.info("serving at %s with %s backend", url, backend.lower())
        match BackendType[backend]:
            case BackendType.MOCK:
                from .backends.mock import MockBackend

                self._backend = MockBackend(mock_channel_file)
            case BackendType.CLICKHOUSE:
                from .backends.clickhouse import ClickHouseBackend

                self._backend = ClickHouseBackend(backend_server_url)
            case BackendType.NDS2:
                from .backends.nds import NDS2Backend

                self._backend = NDS2Backend(backend_server_url)
            case BackendType.HYBRID:
                from .backends.hybrid import HybridBackend

                self._backend = HybridBackend(backend_server_url)

    def list_flights(
        self, context: flight.ServerCallContext, criteria: bytes
    ) -> Iterator[flight.FlightInfo]:
        """List flights available on this service.

        Parameters
        ----------
        context : ServerCallContext
            Common contextual information.
        criteria : bytes
            Filter criteria provided by the client.

        Yields
        ------
        FlightInfo

        """
        logger.debug("serving list_flights for %s", context.peer())
        for channel in self._channels:
            yield self._make_flight_info(
                create_command(RequestType.Stream, channels=[channel.name])
            )

    def get_flight_info(
        self, context: flight.ServerCallContext, descriptor: flight.FlightDescriptor
    ) -> flight.FlightInfo:
        """Get information about a flight.

        Parameters
        ----------
        context : ServerCallContext
            Common contextual information.
        descriptor : FlightDescriptor
            The descriptor for the flight provided by the client.

        Returns
        -------
        FlightInfo

        """
        logger.debug("serving get_flight_info for %s", context.peer())
        return self._make_flight_info(descriptor.command)

    def do_exchange(
        self,
        context: flight.ServerCallContext,
        descriptor: flight.FlightDescriptor,
        reader: flight.MetadataRecordBatchReader,
        writer: flight.MetadataRecordBatchWriter,
    ) -> flight.FlightDataStream:
        """Write data to a flight.

        Parameters
        ----------
        context : ServerCallContext
            Common contextual information.
        ticket : Ticket
            The ticket for the flight.

        Returns
        -------
        FlightDataStream
            A stream of data to send back to the client.

        """
        request, kwargs = parse_command(descriptor.command)
        match request:
            case RequestType.Partition:
                if not traits.can_publish(self._backend):
                    raise flight.FlightError(
                        "partition not supported for server backend"
                    )
                return self._backend.do_partition(reader, writer, **kwargs)
            case _:
                raise flight.FlightError("request type not valid")

    def do_get(
        self, context: flight.ServerCallContext, ticket: flight.Ticket
    ) -> flight.FlightDataStream:
        """Write data to a flight.

        Parameters
        ----------
        context : ServerCallContext
            Common contextual information.
        ticket : Ticket
            The ticket for the flight.

        Returns
        -------
        FlightDataStream
            A stream of data to send back to the client.

        """
        request, kwargs = parse_command(ticket.ticket)
        logger.debug("serving %s request for %s", request.name, context.peer())
        try:
            match request:
                case RequestType.Stream:
                    return self._backend.do_stream(**kwargs)
                case RequestType.Describe:
                    return self._backend.do_describe(**kwargs)
                case RequestType.Find:
                    return self._backend.do_find(**kwargs)
                case RequestType.Count:
                    return self._backend.do_count(**kwargs)
                case RequestType.Publish:
                    if not traits.can_publish(self._backend):
                        raise flight.FlightError(
                            "publish not supported for server backend"
                        )
                    return self._backend.do_publish(**kwargs)
                case _:
                    raise flight.FlightError("request type not valid")
        except Exception:
            logger.exception(
                "error while processing %s request for %s", request.name, context.peer()
            )
            raise flight.FlightError("internal server error")

    def list_actions(
        self, context: flight.ServerCallContext
    ) -> Iterable[tuple[str, str]]:
        """List custom actions available on this server.

        Parameters
        ----------
        context : ServerCallContext
            Common contextual information.

        Returns
        -------
        Iterable of 2-tuples in the form (command, description).

        """
        logger.debug("serving list_actions for %s", context.peer())
        return [("publish", "Request to publish data.")]

    def do_action(
        self, context: flight.ServerCallContext, action: flight.Action
    ) -> Iterator[bytes]:
        """Execute a custom action.

        Parameters
        ----------
        context : ServerCallContext
            Common contextual information.
        action : Action
            The action to execute.

        Yields
        ------
        bytes

        """
        logger.debug("serving %s action for %s", action.type, context.peer())
        match action.type:
            case _:
                raise flight.FlightError("action not valid")

    def _make_flight_info(self, cmd: bytes) -> flight.FlightInfo:
        request, args = parse_command(cmd)
        match request:
            case RequestType.Stream:
                # FIXME: this should query the backend to map channel
                # metadata to their corresponding data types. does
                # this imply that a fetch request requires a
                # find_channels request?
                dtypes = ["float32" for _ in args["channels"]]
                schema = schemas.stream(args["channels"], dtypes)
            case RequestType.Describe:
                schema = schemas.describe()
            case RequestType.Find:
                schema = schemas.find()
            case RequestType.Count:
                schema = schemas.count()
            case RequestType.Publish:
                if not traits.can_publish(self._backend):
                    raise flight.FlightError("publish not supported for server backend")
                schema = schemas.publish()
            case RequestType.Partition:
                if not traits.can_publish(self._backend):
                    raise flight.FlightError(
                        "partition not supported for server backend"
                    )
                schema = schemas.partition()
            case _:
                raise flight.FlightError("command not understood")

        descriptor = flight.FlightDescriptor.for_path(cmd)
        endpoints = [flight.FlightEndpoint(cmd, [self._location])]
        return flight.FlightInfo(schema, descriptor, endpoints, -1, -1)

    def finalize(self) -> None:
        """Do any final processing before shutting down the server."""
        self._backend.finalize()
