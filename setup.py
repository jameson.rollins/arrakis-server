from setuptools import setup

setup(use_scm_version={"write_to": "arrakis_server/_version.py"})
